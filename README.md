# Git Training

This training is made to discover `git` by practicing. Slides are made to guide the speaker. They are not auto-explicatives, but content some times speaker notes.

[Slides](slides.md) have been made with [reveal-md](https://github.com/webpro/reveal-md), a Markdown layer over [reveal.js](https://revealjs.com/).  
Serve slides locally with:
```shell
reveal-md slides.md
```

## Prerequisites
- Internet connection
- SSH key linked to the gitlab account
- Git installed

## Table of content:
- Introduction to version control
- Working locally
    + [Exercise 1](exercise1.md)
    + Setting up a repository
    + Saving changes
    + Inspecting a repository
    + Syncing
    + Advanced Git log
    + Amend a commit
    + .gitignore
    + Conventionnal commit messages
- Branches
    + [Exercise 2](exercise2.md)
    + Creating
    + Switching
    + Merging
    + Rebasing
    + Merging vs. Rebasing
    + Git pull usage
- Interactive rebase
    + [Exercise 3](exercise3.md)
- Worflows
    + Centralized workflow
    + Feature Branch workflow
    + Forking workflow
    + Gitflow workflow
- Remotes
- Refs
- Undoing changes
    + Reverting
    + Checking out vs. Resetting
    + Resetting
- Extra
    + Git stash
    + Cherry-pick
    + Oh Shit, Git !?!
- Links and Tools

## Credits
Authors: Etienne Profit, Julien Teigny, Julien Vandaele.  
_Most of the content and all figures are taken from the Atlassian Guide about git: [Getting Git Right](https://www.atlassian.com/git), distributed under a [CC BY 2.5 AU](https://creativecommons.org/licenses/by/2.5/au/) licence._
