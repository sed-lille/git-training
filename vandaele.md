# Julien Vandaele
**Research engineer**  
_SED_  
[website](https://www.inria.fr)

## Dev
- Java
- Python
- C
- Git
  - GitHub
  - gitlab
- Docker
My favorite linux command is `git stash`.

## My top 3 tv shows
1. House of cards
1. Narcos
1. Peaky Blinders

## My top 3 emoticons
:smile: - :heart: - :shit:

Emoticons code is:
```
:smile: - :heart: - :shit:
```
