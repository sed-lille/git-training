# Exercise 3: Interactive rebase

**Purpose:** edit, reorder and squash commits.  
**Objective:** learn interactive mode of git rebase.

_Run the `create-rebasing-branch.sh` script._
```
./create-rebasing-branch.sh path/to/your/local/git-sandbox/
```

```shell
$ git pull
$ git switch rebasing
$ git log -p main..rebasing
$ git rebase -i main
```

Edit commands from:
```
pick 4c6e7e3 one
pick ba2e32a threeeeee
pick 50dbd1a two
pick 20c4614 two bis
```
to:
```
pick 4c6e7e3 one
pick 50dbd1a two
f 20c4614 two bis
r ba2e32a threeeeee
```

Check the result:
```shell
$ git log --oneline main..rebasing
718b921 (HEAD -> rebasing) three
1ea1d50 two
4c6e7e3 one
```

**Extra question**  
The `threeeeee.md` file still has a bad name. How to correct and include this change to the history without adding an additionnal commit ?  
```shell
$ git mv threeeeee.md three.md
$ git commit --amend --no-edit
```