# Exercise 1: Résumé

**Purpose:** produce a text file containing the attendee's résumé. Firstly written in simple text ([example here](vandaele.txt)), the file is then transformed in a Markdown file ([example here](vandaele.md)) and finally published to the main repository.  
**Objective:** discover git commands to work locally.

## Initialisation
Create the git-sandbox project on gitlab:
- visibility: public
- initialize repository with a README

```shell
$ git clone <URL>
$ cd git-sandbox
$ touch <login>.txt
$ git add <login>.txt
$ git commit
# edit commit message with "add <login>'s file"
$ git log
# explanation about commit information
```

## Header section
```shell
# edit <login>.txt
$ git add <login>.txt
$ git commit -m "header"
$ git log
```

## Dev section
```shell
# edit <login>.txt
$ git status
# explanation about the 3 trees
$ git add <login>.txt
$ git status
$ git commit -m "dev"
$ git status
$ git log
```

## TV section
```shell
# edit <login>.txt
$ git status
$ git diff
$ git commit -a -m "tv"
```

---
Give the printed PDF to attendees, to produce their Résumé.  
Ask them to _Request Access_ to the gitlab project to be added as Developper, and be able to push to the repository later on.

Attendees do
- Request access & clone the git-sandbox project
- Produce 4 commits (creation, header, dev, tv)
---

## Markdown syntax
```shell
# explanation about the Markdown syntax
# use VS code for edit and preview
$ git mv <login>.txt <login>.md
$ git commit -a -m "markdown syntax"
$ git push
```
My résumé is avaiable online, with an HTML preview.

---

_Modify Protected branch rule to allow developpers to push._

Attendees do
- Transform to Markdown file with new commit(s)
- Push
---

## Emoticons section
```shell
# edit <login>.md
# use VS Code for pull, status, diff, add, commit and push.
```
_Run the `produce-conflict.sh` script, to modify attendees' Résumé before publishing their last section._
```
./produce-conflict.sh path/to/your/local/git-sandbox/
```

---
Attendees do
- Add the emoticons section with new commit(s)
- Push again
- Resolve conflict
---
