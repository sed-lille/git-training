# Exercise 2: Mr. Men Little Miss

**Purpose:** produce, by pairs, characters file from the [Mr. Men Little Miss](https://mrmen.com/pages/characters) collection.  
**Objective:** discover git commands to work with branches.

_Set Protected branch rule to default (push allowed only to maintainers)._

In parallel:
- `A` works on `<character>` branch for text content
    ```shell
    $ git pull
    $ git branch
    $ git branch <character>
    $ git branch
    $ git switch <character>
    # edit <character>.md
    $ git status
    $ git add <character.md>
    $ git commit -m "text file for <character>"
    $ git push
    $ git push --set-upstream origin <character>
    ```
- `B` works on `<character>-image` branch for image content
    ```shell
    $ git pull
    $ git switch -c <character>-image
    $ mkdir images
    # get png picture of character as images/<character>.png
    $ git status
    $ git add images/<character>.png
    $ git commit -m "image of <character>"
    $ git push --set-upstream origin <character>-image
    ```
Then, `A` merges `<character>-image` in `<character>` and pushes
```shell
$ git fetch
$ git branch
$ git branch -a
$ git merge origin/<character>-image
$ git push
```
`A` merges `<character>` in `main` and pushes
```shell
$ git switch main
$ git merge <character>
$ git push
# explanation about protected branch
# create merge request via gitlab interface
```