#!/bin/bash
cd $1
git switch main
git pull --rebase
git switch -c rebasing

newfile="one"
content="1111111111"
touch $newfile.md
echo $content > $newfile.md
git add $newfile.md
git commit -m "$newfile"

newfile="threeeeee"
content="3333333333"
touch $newfile.md
echo $content > $newfile.md
git add $newfile.md
git commit -m "$newfile"

newfile="two"
content="2222222222"
touch $newfile.md
git add $newfile.md
git commit -m "$newfile"

echo $content > $newfile.md
git commit -a -m "$newfile bis"

git switch main
git push origin rebasing