#!/bin/bash
cd $1
git pull --rebase
for entry in *.md
do
    printf "\n---\nwritten for the Git training on $(date)" >> $entry
done
git commit -a -m "time-stamping"
git push
