#!/bin/bash
cd $1

git pull --rebase
gitignore_file=".gitignore"
touch $gitignore_file
echo "*.html" > $gitignore_file
git add $gitignore_file
git commit -m "add the $gitignore_file"
git push
